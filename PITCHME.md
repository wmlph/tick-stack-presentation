## Simple Monitoring

### Using the Influx TICK stack

14 Sep 2018

Phil Collins & Adam Sandler

Development Team

---

### Preamble

* Please feel free to ask questions, just don't expect answers.
* We are in no way experts of this topic.  We are beginner users, noobs at best.
* We are being held captive by an evil middle management overlord who forces us to give presentations, please call the relevant authorities!  

---

### What is TICK?

InfluxData TICK provides a Modern Time Series Platform, designed from the ground up to handle metrics and events.

InfluxData's products are based on an open source core. 

This open source core consists of various projects collectively called the TICK Stack.

---

### What does the TICK stack contain?

![image](stack.png)

@color[red](T)elegraf collects & distributes, @color[red](I)nfluxDB stores & downsamples, @color[red](C)hronograf manages, @color[red](K)apacitor alerts

---

## Telegraf

Telegraf is a plugin-driven server agent for collecting and reporting metrics. 

Telegraf has integrations to source metrics, events, and logs directly from the containers and systems it's running on, pull metrics from APIs, or even listen for metrics via a StatsD and Kafka consumer services. 

It also has output plugins to send metrics to many other datastores, services, and message queues.

---

### InfluxDB

InfluxDB is a data store for any use case involving large amounts of time-stamped data, including DevOps monitoring, log data, application metrics, IoT sensor data, and real-time analytics.

Conserve space by configuring InfluxDB to keep data for a defined length of time, automatically expiring & deleting any unneeded data from the system. 

InfluxDB also offers a SQL-like query language for interacting with data.

---

### Example Query

```sql
select 
    first(rate) as open, 
    max(rate) as high, 
    min(rate) as low, 
    last(rate) as close, 
    exponential_moving_average(rate,14),
    market 
from prices 
group by market, time(5m) 
order by market
```

Many other statistical functions are available, including bottom, percentile, moving_average, log10, log2, holt_winters, sqrt, histogram etc.

---

### Chronograf

Chronograf is the user interface component of InfluxData's TICK Stack. 

It allows you to quickly see the data that you have stored in InfluxDB so you can build robust queries and alerts. 

@ul

- but... we don't use Chronograf, we use Grafana, of which more later.

@ulend

---

### Kapacitor

Kapacitor is a native data processing engine in the TICK Stack. It can process both stream and batch data from InfluxDB. 

It lets you plug in your own custom logic or user-defined functions to process alerts with dynamic thresholds, match metrics for patterns, compute statistical anomalies, and perform specific actions based on these alerts . 

It also integrates with Slack and many others.

---

### TICK Sandbox

The TICK sandbox is a quick way to get the entire TICK stack spun up and working together.  It uses docker to spin up the TICK stack in a connected fashion.

Running this gives you a complete stack that simply needs to be configured.

Find the TICK sandbox on github:

 [TICK Sandbox -- https://github.com/influxdata/sandbox](https://github.com/influxdata/sandbox)


---

### And now over to Adam for a quick demo.

--- 

### Grafana

Not part of TICK, but used as an alternative to Chronograf.  It is the leading open source software for time series analytics.

Grafana allows you to query, visualize, alert on and understand your metrics no matter where they are stored. Create, explore, and share dashboards with your team and foster a data driven culture.

As of right now, there are 45 data sources, 37 panels, 16 apps and 1166 example dashboards available.

---
### How to add new endpoints

Endpoints can be easily added, removed or modified by altering the telegraf.conf file that resides on the monitoring station. 
---
### HTTP Response 

```
[[inputs.http_response]]
        address = "http://www.cardiff.ac.uk/"
        interval = "60s"
        method = "GET"
```
---
### JSON REST API

```
    [[inputs.httpjson]]
        name = "opendaydata"
        servers = [
            "https://opendaydata.cardiff.ac.uk/en/api/versions",
            "https://opendaydata.cardiff.ac.uk/en/api/getLocations",
        ]
        method = "GET"
        [inputs.httpjson.headers]
            authorization =  "bearer bb6f06997f2458fe1abd32f827a8c28d13f3444e"
        interval = "30s"
        data_format = "JSON"
```
---
### Restarting

Once the desired changes have been made to the telegraf.conf file the docker sandbox needs to be restarted
```bash
$ cd sandbox
$ ./sandbox restart
```
and restart Grafana as it is not part of the docker sandbox

```bash
$ sudo service grafana-server start
```

---

### And goodbye from us

Any questions?  

@size[48px](This is where you say no!)