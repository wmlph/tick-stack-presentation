# Monitoring

* hello
  - feel free to ask questions
  - noobs, limited experience but see the worth
  - running since end of March in it's current guise
  - running in some form for a few years longer

* What is TICK stack
  - Why I personally started using Influxdb. Trading bitcoin et al.
  - Go language
  - grew up alongside docker, kubernetes etc...
  - ecosystem grew with the addition of other tools
  - ![image](stack.png)

* what TICK contains
  - quickly move on after reading the slide

* Telegraf
  - Agent: Telegraf is a metric collection daemon that can collect metrics from a wide array of inputs and write them into a wide array of outputs. It is plugin-driven for both collection and output of data so it is easily extendable. It is written in Go, which means that it is a compiled and standalone binary that can be executed on any system with no need for external dependencies, no node npm, python pip, ruby gem, or other package management tools required.
  - Databases: MongoDB, MySQL, Redis, Oracle, et al. for both collection and sending metrics
  - collect metrics from modern stack of cloud platforms, containers and orchestrators (but also from bare machines)
  - IoT sensors: state data from IoT devices and sensors (temperature, pressure, whatever sensors you have)
  - Plugins: including JSON, Graphite, Nagios, and Collectd.

* Influxdb
  - High Performance
  - SQL-like queries
  - downsampling and data retention
  - clustering 

* chronograf
  - dashboards, visulaizing data, pre-canned examples
  - administration, management of other parts of the stack.
  - security: RBAC, GitHub, googlem Auth0 etc...
  - alerting: step-by-step ui for creating alerts, alert history

* kapacitor
  - alerts: Today’s modern applications require more than just dashboarding and operator alerts—they need the ability to trigger actions. Kapacitor’s alerting system follows a publish-subscribe design pattern. Alerts are published to topics and handlers subscribe to a topic. This pub/sub model and the ability for these to call User Defined Functions make Kapacitor very flexible to act as the control plane in your environment, performing tasks like auto-scaling, stock reordering, and IoT device control.
  - streaming analytics: real-time data
  - pre-procesor: downsample and analyse before sending to influxdb
  - anomaly detection, machine learning integration

* TICK Sandbox
  - self contained
  - requires docker 
  - useful for testing the stack and learning how it all works
  - documentation built in

## Now over to Adam.

* Grafana
  - Visualize
  - alert
  - unify
  - cross-platform
  - pre-built dashboards
  - plugins
  - collaboration
  - data sources: influxdb, Carbon, graphite, elasticsearch
  - authentication: LDAP, Google OAuth, GitHub OAuth
  - Organizations: multi-tenancy, multiple organizations
  - user preferences: theme, home dashboard, timezones
